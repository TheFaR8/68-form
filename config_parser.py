from werkzeug.utils import import_string


def parse_config(obj):
    """
    Pengaturan dalam perbaikan kesalahan sintaksis baik dari file
    maupun dari objek. Metode ini dipinjam dari Flask.

    parameter:
    obj -> pengaturan (tipe data: apapun)
    return -> dictionary dari pengaturan flask yang sudah dibenahi (tipe data: dict)
    """
    config = {}
    if isinstance(obj, str):
        obj = import_string(str)
    for key in dir(obj):
        if key.isupper():
            config[key] = getattr(obj, key)
    return config
