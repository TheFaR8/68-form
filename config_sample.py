# For manual installation, fill in the fields below. If you are using
# install.sh, the config.py should have been generated for you.
APPLICATION_ROOT = None
CSRF_ENABLED = True
DATABASE_URI = ''
SERVER_NAME = 'localhost'
EMAIL_DOMAIN = ''
EMAIL_API_KEY = ''
INSTALL_FOLDER = '/path/to/installation'
SESSION_COOKIE_PATH = '/'
FTP_PORT = 21
MAX_CONTENT_LENGTH = 512 * 1024 * 1024
MIN_PWD_LEN = 6
MAX_PWD_LEN = 20
DEBUG = False
SERVER_PORT = '0.0.0.0'
