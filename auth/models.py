"""
auth.model
===========
Dalam module ini, diusahakan untuk mengatur semua model database
yang digunakan dalam autentikasi
Daftar model menurut tabel mysql: ['User' -> 'user']
"""

from passlib.apps import custom_app_context as pwd_context
from sqlalchemy import Column, Integer, String

from database import Base, DeclEnum


class Role(DeclEnum):
    admin = "admin", "Admin"
    user = "user", "User"


class User(Base):
    __tablename__ = 'user'
    __table_args__ = {'mysql_engine': 'InnoDB'}
    id = Column(Integer, unique=True, primary_key=True)  # id ini NIS
    name = Column(String(60), unique=True)
    email = Column(String(50), unique=True, nullable=False)
    password = Column(String(255), unique=False, nullable=False)
    kelas = Column(String(5), nullable=False)
    wa = Column(Integer, unique=True, nullable=True)  # nomor wa
    jurusan1 = Column(String(60), unique=False)
    jurusan2 = Column(String(60), unique=False)
    jurusan3 = Column(String(60), unique=False)
    role = Column(Role.db_type())

    def __init__(self, id, name, role=Role.user, email=None, password=''):
        """
        Pembangun berparameter untuk model User

        parameter:
        name -> kolom nama (tipe: string)
        id -> NIS (tipe: integer)
        role -> peran dari akun (admin atau user biasa) (tipe: Role)
        email -> email pengguna (tipe: string)
        password -> password pengguna (tipe: string)
        """
        self.id = id
        self.name = name
        self.email = email
        self.role = role
        self.password = password

    def __repr__(self):
        """
        Fungsi yang mereprentasikan model user dari kolom yang diisi mereka

        return:
        str(name) -> String berupa nama dari kolom nama dari model pengguna
        tipe: string
        """
        return '<{name}>'.format(self.name)

    @staticmethod
    def generate_hash(password):
        """
        Generates a Hash value for a password
        :param password: The password to be hashed
        :type password: str
        :return : The hashed password
        :rtype : str
        """
        # Go for increased strength no matter what
        return pwd_context.encrypt(password, category='admin')

    @staticmethod
    def is_password_valid(self, password):
        """
        Cek validitas password

        parameter:
        password -> password yg dimasukkan oleh user (tipe data: string)
        return: validitas password (tipe data: boolean)
        """
        return pwd_context.verify(password, self.password)

    def update_password(self, new_password):
        """
        Update password ke yang baru

        parameter:
        new_password -> password baru (tipe data: string)
        """
        self.password = self.generate_hash(new_password)

    @property
    def is_admin(self):
        """
        Verifies if a User is the admin
        :return : Checks if User has an admin role
        :rtype: boolean
        """
        return self.role == Role.admin

    def has_role(self, name):
        """
        Checks whether the User has a particular role
        :param name: Role of the user
        :type name: str
        :return : Checks whether a User has 'name' role
        :rtype: boolean
        """
        return self.role.value == name or self.is_admin
