from flask_wtf import FlaskForm
from wtforms import PasswordField, StringField, SubmitField, SelectField, BooleanField
from wtforms.fields.html5 import EmailField
from wtforms.validators import DataRequired, Email, ValidationError

from auth.models import User, Role


def unique_nis(form,field):
    """
    Cek apabila adakah pengguna dengan nomor nis ini
    Apabila ada yang ingin mengganti nisnya, mohon menghubungi Tim IT

    parameter:
    form -> form yang diisi oleh pengguna (tipe: Form)
    field -> Nilai data untuk diisi oleh user baru (tipe: StringField)
    """
    user = User.query.filter(User.id == field.data).first()
    if user is None:
        raise ValidationError('Sudah ada yang menggunakan nomor NIS ini. \
                               Jika Anda pemilik dari nomor NIS ini, silakan hubungi tim IT (Theo/Arya)')


def valid_password(form, field):
    """
    Function to check for validity of a password
    :param form: The form which is being passed in
    :type form: Form
    :param field: The data value for the 'password' inserted by User
    :type field : PasswordField
    """
    from run import config
    min_pwd_len = int(config['MIN_PWD_LEN'])
    max_pwd_len = int(config['MAX_PWD_LEN'])
    pass_size = len(field.data)
    if pass_size == 0:
        raise ValidationError('new password cannot be empty')
    if pass_size < min_pwd_len or pass_size > max_pwd_len:
        raise ValidationError(
            'Password harus memiliki setidaknya di antara {min_pwd_len} - {max_pwd_len} karakter \
            (anda telah memasukkan {char} karakter)'.format(
                min_pwd_len=min_pwd_len, max_pwd_len=max_pwd_len, char=pass_size)
            )


def email_not_in_used(has_user_field=False):
    """
    Memastikan apakah email yang dimasukkan sudah pernah dipakai atau belum

    parameter:
    has_user_field -> Cek apakah email sudah digunakan oleh user yang sudah ada (default=False) (tipe : Boolean)
    """
    def _email_not_in_used(form, field):
        user_id = -1 if not has_user_field else form.user.id
        # Cek apakah email sudah tidak digunakan
        user = User.query.filter(User.email == field.data).first()
        if user is not None and user.id != user_id and len(field.data) > 0:
            raise ValidationError('Email ini sudah digunakan')

    return _email_not_in_used


def role_id_is_valid(form, field):
    """
    Checks for validity of User's Role
    :param form: The form which is being passed in
    :type form: Form
    :param field : The data value for the 'role' inserted by User
    :type field : SelectField
    """
    role = Role.query.filter(Role.id == field.data).first()
    if role is None:
        raise ValidationError('Role id is invalid')


class LoginForm(FlaskForm):
    """
    Form akan dirender saat user akan masuk
    """
    nis = StringField('NIS', [
        DataRequired(message='NIS belum diisi')
    ])
    password = PasswordField('Password', [DataRequired(message='Password ini tidak bisa kosong')])
    ingat_saya = BooleanField('Ingat saya')
    submit = SubmitField('Masuk')


class SignupForm(FlaskForm):
    """
    Form akan dirender saat user akan mendaftar
    """
    email = EmailField('Email', [
        DataRequired(message='Alamat email belum diisi'),
        Email(message='Data yang dimasukkan bukanlah alamat email yang valid')
    ])
    submit = SubmitField('Daftar')


class RoleChangeForm(FlaskForm):
    """
    Changing the Role
    """
    role = SelectField('Select a role', [DataRequired(message='Role is not filled in.')], coerce=str)
    submit = SubmitField('Change role')


class CompleteSignupForm(FlaskForm):
    """
    Form untuk mendaftar secara lengkap (sebelumnya itu hanya mengirim link kepada user untuk link ini)
    """
    name = StringField('Nama', [DataRequired(message='Nama belum diisi')])
    nis = StringField('NIS', [DataRequired(message='NIS belum diisi')])
    password = PasswordField('Password', [DataRequired(message='Password belum diisi'), valid_password])
    password_repeat = PasswordField('Ulangi Password', [DataRequired(message='Password belum diisi')])
    submit = SubmitField('Daftar')

    @staticmethod
    def validate_repeat_password(form, field):
        """
        Memvalidasi password ulang
        """
        if field.data != form.password.data:
            raise ValidationError('Password tidak sesuai')


class AccountForm(FlaskForm):
    """
    Formulir untuk mengubah informasi akun
    """

    def __init__(self, formdata=None, obj=None, prefix='', *args, **kwargs):
        super(AccountForm, self).__init__(formdata=formdata, obj=obj, prefix=prefix, *args, *kwargs)
        self.user = obj

    current_password = PasswordField('Password lama', [DataRequired(message='Password lama tidak bisa kosong')])
    new_password = PasswordField('Password baru', [DataRequired(message='Password baru tidak bisa kosong')])
    new_password_repeat = PasswordField('Ulangi password baru')
    name = StringField('Nama', [DataRequired(message='Nama tidak bisa dikosongi')])
    nis = StringField('NIS', [DataRequired(message='NIS tidak b isa dikosongi (mohon tidak mengganti NIS dengan sembarang nomor')])
    email = EmailField('Email', [
        DataRequired(message='Alamat email tidak bisa kosong'),
        Email(message='Bukan alamat email yang valid'),
        email_not_in_used(True)
    ])
    jurusan_1 = StringField('Masukkan pilihan pertama Anda dengan format: [jurusan](inisial Universitas)', [DataRequired(message='Setidaknya Anda harus memilih 1 jurusan')])
    jurusan_2 = StringField('Masukkan pilihan kedua Anda dengan format: [jurusan](inisial Universitas)')
    jurusan_3 = StringField('Masukkan pilihan ketiga Anda dengan format: [jurusan](inisial Universitas)')
    kelas = SelectField('Kelas', [DataRequired(message='Kelas belum diisi. Isilah dengan format di samping(contoh: XII IPA 3)')])
    wa = StringField('Nomor whatsapp', [DataRequired(message='Untuk kemudahan mengurus akun maupun update informasi, mohon isi kolom ini')])
    submit = SubmitField('Simpan')

    @staticmethod
    def validate_current_password(form, field):
        """
        Memvalidasi password sekarang dengan password yang ada di database
        :param form: The form which is being passed in
        :type form: AccountForm
        :param field: The data value for the 'password' entered by User
        :type field : PasswordField
        """
        if form.user is not None:
            if not form.user.is_password_valid(field.data):
                raise ValidationError('Invalid password')
        else:
            raise ValidationError('User instance not passed to form validation')

    @staticmethod
    def validate_new_password(form, field):
        """
        Validates the new password entered
        :param form: The form which is being passed in
        :type form: AccountForm
        :param field: The data value for the 'password' entered by User
        :type field : PasswordField
        """
        if len(field.data) == 0 and len(form.new_password_repeat.data) == 0:
            return

        valid_password(form, field)

    @staticmethod
    def validate_new_password_repeat(form, field):
        """
        Memvalidasi apakah password baru telah benar
        :param form: The form which is being passed in
        :type form: AccountForm
        :param field: The data value for the 'password' entered by User
        :type field : PasswordField
        """
        if form.email is not None:
            # Email form is present, so it's optional
            if len(field.data) == 0 and len(form.new_password.data) == 0:
                return

        if field.data != form.new_password.data:
            raise ValidationError('The password needs to match the new password')

    @staticmethod
    def no_jurusan_repeat(form, field):
        """
        Memastikan kalau jurusan yang dimasukkan itu tidak sama antara jurusan 1,2 dan 3
        :param form: Form yang akan diisi oleh user
        :type form: AccountForm
        :param field: Data yang dimasukan oleh user
        :type form: StringField
        """
        if form.jurusan1 is form.jurusan2 or form.jurusan3:
            raise ValidationError('Jurusan atau universitas harus berbeda satu sama lain')


class ResetForm(FlaskForm):
    """
    Mengirim email yang terlampir link reset password
    """
    email = EmailField('Email', [
        DataRequired(message='Kolom tidak bisa kosong'),
        Email(message='Email tidak valid')
    ])
    submit = SubmitField('Kirim email instruksi reset')


class CompleteResetForm(FlaskForm):
    """
    Reset password setelah men-click link dalam email
    """
    password = PasswordField('Password',[DataRequired(message='Password baru tidak bisa kosong'), valid_password])
    password_repeat = PasswordField('Ulangi Password', [DataRequired(message='Kolom ini tidak bisa dikosongi')])
    submit = SubmitField('Reset')

    @staticmethod
    def validate_password_repeat(form, field):
        """
        Memvalidasi password yang telah diketik ulang
        """
        if field.data != form.password.data:
            raise ValidationError('Password harus sesuai dengan password yang baru')


class JurusanForm(FlaskForm):
    """
    Form untuk memilih jurusan
    """
    jurusan1 = StringField('Pilihan Jurusan 1', [DataRequired(message='Setidaknya anda memilih satu jurusan')])
    jurusan2 = StringField('Pilihan Jurusan 2', [DataRequired(message='Kolom ini tidak bisa kosong. Jika tidak ingin menambahkan\
    silahkan isi dengan "-"')])
    jurusan3 = StringField('Pilihan Jurusan 3', [DataRequired(message='Kolom ini tidak bisa kosong. Jika tidak ingin menambahkan\
    silahkan isi dengan "-"')])
    submit = SubmitField("Update")

    @staticmethod
    def no_jurusan_repeat(form, field):
        """
        Memastikan kalau jurusan yang dimasukkan itu tidak sama antara jurusan 1,2 dan 3
        :param form: Form yang akan diisi oleh user
        :type form: AccountForm
        :param field: Data yang dimasukan oleh user
        :type form: StringField
        """
        if form.jurusan1 is form.jurusan2 or form.jurusan3:
            raise ValidationError('Jurusan atau universitas harus berbeda satu sama lain')
