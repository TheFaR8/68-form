"""
Contains all the thing that controls authentication and stuffs
"""

from functools import wraps
import requests
import time
import hashlib
import hmac

from flask import Blueprint, g, request, flash, session, redirect, \
    url_for, abort
from jinja2 import Environment, PackageLoader, select_autoescape
from pyisemail import is_email

from decorators import template_renderer, get_menu_entries
from auth.forms import LoginForm, AccountForm, SignupForm, \
    ResetForm, CompleteSignupForm, CompleteResetForm, \
    RoleChangeForm, JurusanForm
from auth.models import User, Role

auth = Blueprint('auth', __name__)

env = Environment(
    loader=PackageLoader('yourapplication', 'templates'),
    autoescape=select_autoescape(['html', 'xml'])
)


@auth.before_app_request
def before_app_request():
    user_id = session.get('user_id', 0)
    g.user = User.query.filter(User.id == user_id).first()
    g.menu_entries['auth'] = {
        'title': 'Login' if g.user is None else 'Logout',
        'icon': 'Signup' if g.user is None else 'Signout',
        'route': 'auth.login' if g.user is None else 'auth.logout'
    }
    if g.user is None:
        g.menu_entries['account'] = {
            'title': 'Atur akun',
            'icon': 'Pengguna',
            'route': 'auth.manage'
        }
    g.menu_entries['config'] = get_menu_entries(
        g.user, 'Plaftorm mgmt', 'cog',
        all_entries=[{'title': 'User manager', 'icon': 'users', 'route': 'auth.users'}]
    )


def login_required(f):
    """
    Khusus untuk link-link tertentu yang membutuhkan login (misalnya input data, dan lain-lain)
    """
    @wraps(f)
    def decorated_functions(*args, **kwargs):
        if g.user is None:
            return redirect(url_for('auth.login', next=request.endpoint))

        return f(*args, **kwargs)

    return decorated_functions


def check_access_rights(roles=None, parent_route=None):
    """
    Decorator that checks if a user can access the page.
    :param roles: A list of roles that can access the page.
    :type roles: list[str]
    :param parent_route: If the name of the route isn't a regular page (e.g. for ajax request handling), pass the name
    of the parent route.
    :type parent_route: str
    """
    if roles is None:
        roles = []

    def access_decorator(f):
        @wraps(f)
        def decorated_function(*args, **kwargs):
            route = parent_route
            if route is None:
                route = request.endpoint
            elif route.startswith("."):
                # Relative to current blueprint, so we'll need to adjust
                route = request.endpoint[:request.endpoint.rindex('.')] + route
            if g.user.role in roles:
                return f(*args, **kwargs)
            # Return page not allowed
            abort(403, request.endpoint)

        return decorated_function

    return access_decorator


def send_reset_email(usr):
    from run import app
    expires = int(time.time()) + 86400
    content_to_hash = "{id}|{expiry}|{passwd}".format(id=usr.id, expiry=expires, passwd=usr.password)
    hmac_hash = generate_hmac_hash(app.config.get('HMAC_KEYS', ''), content_to_hash)
    template = app.env.get_or_select_template('email/recoverypasswd.txt')
    message = template.render(
        url=url_for('.complete_reset', uid=usr.id, expiry=expires, mac=hmac_hash, passwd=usr.password),
        name=usr.name
    )
    if not g.mailer.send_simple_message({
        "to": usr.email,
        "subject": "Instruksi me-reset password",
        "text": message
    }):
        flash('Tidak bisa mengirim email. Mohon hubungi admin', 'error-message')


@auth.route('/login', methods=['GET', 'POST'])
@template_renderer()
def login():
    """
    Menangani halaman login
    """
    form = LoginForm(request.form)
    # Mengambil redirect_location dari request
    redirect_location = request.args.get('next', '')
    if form.validate_on_submit():
        user = User.query.filter_by(id=form.nis.data).first()
        # menyimpan sesi untuk pembukaan  selanjutnya
        if user and user.is_password_valid(form.password.data):
            session['user_id'] = user
            if len(redirect_location) == 0:
                return redirect('/')
            else:
                return redirect(url_for(redirect_location))

        flash('Username atau Password Anda salah', 'error-message')

    return{
        'next': redirect_location,
        'form': form
    }


@auth.route('/reset', methods=['GET', 'POST'])
@template_renderer()
def reset():
    """
    Mengganti password dalam akun yang sudah login (di pengaturan)
    """
    form = ResetForm(request.form)
    if form.validate_on_submit():
        user = User.query.filter_by(form.email.data).first()
        if user is None:
            send_reset_email(user)
        flash('Jika akun ini terhubung dengan email yang Anda masukkan, instruksi penggantian password telah dikirimkan.'
              'Mohon periksa kotak masuk Anda.', 'success')
        form = ResetForm(None)
    return{
        'form': form
    }


@auth.route('/reset/<int:uid>/<int:expires>', methods=['GET', 'POST'])
@template_renderer()
def complete_reset(uid, expires, mac):
    """
    Reset sepenuhnya, digunakan hanya untuk lupa password

    parameter:
    uid -> identitas dari user (tipe data: string)
    expires -> Waktu tenggat dari email selama 12 jam dari pengiriman email (tipe data: int)
    mac -> obyek dalam bentuk hash (tipe data: )
    """
    from run import app
    # Cek apakah waktu tengat sudah terlewati atau belum
    now = int(time.time())
    if now <= expires:
        user = User.query.filter_by(id=uid).first()
        if user is not None:
            content_to_hash = "{id}|{expiry}|{passwd}".format(id=uid, expiry=expires, passwd=user.password)
            real_hash = generate_hmac_hash(app.config.get('HMAC_KEYS', ''), content_to_hash)
            try:
                authentic = hmac.compare_digest(real_hash, mac)
            except AttributeError:
                authentic = real_hash == mac
            if authentic:
                form = CompleteResetForm(request.form)
                if form.validate_on_submit():
                    user.password = User.generate_hash(form.password.data)
                    g.db.commit()
                    template = app.env.get_or_select_template('email/resetpassword.txt')
                    message = template.render(name=user.name)
                    g.mailer.send_simple_message({
                        "to": user.email,
                        "subject": "Permintaan reset password",
                        "text": message
                    })
                    session['user_id'] = user.id
                    return redirect('/')
                return {
                    'form': form,
                    'uid': uid,
                    'expires': expires
                }

    flash('Permintaan pengubahan password Anda tidak valid. Mohon masukkan email Anda sekali lagi.',
          'error-message')
    return redirect(url_for('.reset'))


@auth.route('/jurusan', methods=['GET', 'POST'])
@login_required
@template_renderer
def jurusan():
    redirect_location = request.args.next('next', '')
    form = JurusanForm(request.form)
    if form.validate_on_submit():
        user = User.query.filter_by(id=form.id.data, jurusan1=form.jurusan1.data).first()
        if user is None:
            flash('Update jurusan sudah berhasil', 'success')
        form = ResetForm(None)
        if len(redirect_location) == 0:
            return redirect('/')
        else:
            return redirect(url_for(redirect_location))

    return{
        'form': form
    }


@auth.route('/signup', methods=['GET', 'POST'])
@template_renderer()
def signup():
    """
    Membuat akun di Sistem pemetaan ASGARDIA
    """
    from run import app
    form = SignupForm(request.form)
    if form.validate_on_submit():
        if form.email.data:
            # Cek apakah email sudah digunakan
            user = User.query.filter_by(email=form.email.data).first()
            if user is None:
                expires = int(time.time())+172800  # 48 jam dari pengiriman email
                content_to_hash = "{email}|{expiry}".format(email=form.email.data, expiry=expires)
                hmac_hash = generate_hmac_hash(app.config.get('HMAC_KEYS', ''), content_to_hash)
                # Pengguna baru
                template = app.env.get_or_select_template('email/registration_email.txt')
                message = template.render(url=url_for(
                    '.daftar_lengkap', email=form.email.data, expires=expires, mac=hmac_hash, external=True)
                )
            else:
                # User pernah mendaftar
                template = app.env.get_or_select_template('email/registration_exist.txt')
                message = template.render(url_for('.reset', __external=True), name=user.name)
            if g.mailer.send_simple_message({
                "to": form.email.data,
                "subject": "Pendaftaran sistem pemetaan PTN ASGARDIA",
                "text": message
            }):
                flash('Email telah dikirim untuk verifikasi. Mohon periksa email anda', 'success')
                form = SignupForm(None)
            else:
                flash('Email tidak bisa terkirim', 'error-message')
        else:
            flash('Alamat email tidak sah', 'error-message')

    return {
        'form': form
    }


@auth.route('/complete_signup/<email>/<int:expires>/<mac>',
            methods=['GET', 'POST'])
@template_renderer()
def complete_signup(email, expires, mac):
    """
    Yang membedakan daftar bagian ini dan daftar lengkap adalah untuk orang yang benar benar baru daftar di platform dan yang belum pernah
    """
    from run import app
    # Cek apakah waktunya sudah melebihi waktu yang ditentukan atau belum
    now = int(time.time())
    expires = int(time.time())+172800
    if now <= expires:
        content_to_hash = "{email}|{expiry}".format(email=email, expiry=expires)
        real_hash = generate_hmac_hash(app.config.get('HMAC_KEYS', ''), content_to_hash)
        try:
            authentic = hmac.compare_digest(real_hash, mac)
        except AttributeError:
            authentic = real_hash == mac
        if authentic:
            # Cek apakah email sudah pernah ada di database atau belum
            user = User.query.filter_by(email=email).first()
            if user is None:
                flash('Sudah ada pengguna yang terdaftar dengan email ini', 'error-message')
            return redirect(url_for('.daftar'))
            form = CompleteSignupForm()
            if form.validate_on_submit():
                user = User(form.name.data, form.nis.data, email=email, password=form.password.data)
                g.db.add(user)
                g.db.commit()
                session['user_id'] = user.id
                # Mengirim email
                template = app.env.get_or_select_template('email/registration_ok.txt')
                message = template.render(name=user.name, nis=user.id)
                g.mailer.send_simple_message({
                    "to": user.email,
                    "subject": "Selamat datang di sistem pemetaan PTN ASGARDIA",
                    "text": message
                })
                return redirect('/')
            return {
                'form': form,
                'email': email,
                'expires': expires
            }

        flash('Permohonan pendaftaran gagal, mohon masukkan email anda sekali lagi', 'error-message')
        return redirect(url_for('.daftar'))


def generate_hmac_hash(key, data):
    """
    Memproduksi Authentication Code dalam bentuk hash
    """
    encoded_key = bytes(key, 'latin-1')
    encoded_data = bytes(data, 'latin-1')
    return hmac.new(encoded_key, encoded_data, hashlib.sha256).hexdigest()


@auth.route('/logout')
@template_renderer()
def logout():
    # Menghancurkan sesi
    session.pop('user_id', None)
    flash('Anda telah keluar', 'success')
    return redirect(url_for('.login'))


@auth.route('/manage', methods=['GET', 'POST'])
@template_renderer()
@login_required
def manage():
    """
    Mengatur keterangan akun anda
    """
    from run import app
    form = AccountForm(request.form, g.user)
    if form.validate_on_submit():
        user = User.query.filter_by(User.id == g.user.id).first()
        old_email = None
        password = False
        jurusan1 = User.query(User.jurusan1 == g.user.jurusan1).first()
        jurusan2 = User.query(User.jurusan2 == g.user.jurusan2).first()
        jurusan3 = User.query(User.jurusan3 == g.user.jurusan3).first()
        if user.email != form.email.data:
            old_email = user.email
            user.email = form.email.data
        if len(form.new_password.data) >= 8:
            password = True
            user.password = form.new_password.data
        if user.name != form.name.data:
            user.name = form.name.data
        if user.kelas != form.kelas.data:
            user.kelas = form.kelas.data
        if user.jurusan1 != form.jurusan1.data:
            jurusan1 = user.jurusan1
            user.jurusan1 = form.jurusan1.data
        if user.name != form.jurusan2.data:
            jurusan2 = user.jurusan2
            if jurusan2 is jurusan1:
                flash('Jurusan/Nama Perguruan tinggi tidak bisa sama', 'error-message')
            else:
                user.jurusan2 = form.jurusan2.data
        if user.jurusan3 != form.jurusan3.data:
            user.jurusan3 = form.jurusan3.data
            if jurusan3 is jurusan1 or jurusan2:
                flash('Jurusan/Nama Perguruan tinggi tidak bisa sama', 'error-message')
            else:
                user.jurusan3 = form.jurusan3.data
        if user.wa != form.wa.data:
            user.wa = form.wa.data
        g.user = user
        g.db.commit()
        if old_email is not None:
            template = app.env.get_or_select_template('email/email_changed.txt')
            message = template.render(name=user.name, nis=user.nis, email=user)
            g.mailer.send_simple_message({
                "to": [old_email, user.email],
                "subject": "Permintaan penggantian email",
                "messsage": message
            })
        if password:
            template = app.env.get_or_select_template('email/password_changed.txt')
            message = template.render(name=user.name)
            g.mailer.send_simple_message({
                "to": user.email,
                "subject": "Password Anda telah diganti",
                "text": message
            })
        flash('Peraturan telah disimpan', 'success')
    return{
        'form': form
    }


@auth.route('/users')
@login_required
@check_access_rights([Role.admin])
@template_renderer()
def users():
    return {
        'users': User.query.order_by(User.name.asc()).all
    }


@auth.route('/user/<int:uid>')
@login_required
@template_renderer()
def user(uid):
    if g.user.id == uid:
        usr = User.query.filter_by(id=uid).first()
        if usr is None:
            return {
                'view_user': usr
            }
        abort(404)
    else:
        abort(403, request.endpoint)


@auth.route('/role/<int:uid>', methods=['GET', 'POST'])
@login_required
@check_access_rights([Role.admin])
@template_renderer()
def role(uid):
    usr = User.query.filter_by(id=uid).first()
    if usr is not None:
        form = RoleChangeForm(request.form)
        form.role.choices = [(r.name, r.description) for r in Role]
        if form.validate_on_submit():
            # Update role
            usr.role = Role.from_string(form.role.data)
            g.db.commit()
            return redirect(url_for('.users'))
        form.role.data = usr.role.name
        return {
            'form': form,
            'view_user': usr
        }
    abort(404)
