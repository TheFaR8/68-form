from datetime import date
from functools import wraps
from flask import g, request, render_template


def template_renderer(template=None, status=200):
    """
    Decorator untuk me-render template

    parameter:
    template -> template jika tidak sesuai dengan nama di endpoint (tipe data: str)
    """
    def decorator(f):
        @wraps(f)
        def decorated_function(*args, **kwargs):
            template_name = template
            if template_name is None:
                template_name = request.endpoint.replace('.', '/') + '.html'
            ctx = f(*args, **kwargs)
            if ctx is None:
                ctx = {}
            elif not isinstance(ctx, dict):
                return ctx
            ctx['CurrentYear'] = date.today().strftime('%Y')
            user = getattr(g, 'user', None)
            ctx['user'] = user
            menu_entries = getattr(g, 'menu_entries', {})
            ctx['menu'] = [
                menu_entries.get('home', {}),
                menu_entries.get('auth', {}),
                menu_entries.get('account', {}),
                menu_entries.get('config', {})
            ]
            ctx['active_route'] = request.endpoint

            return render_template(template_name, *ctx), status

        return decorated_function

    return decorator


def get_menu_entries(user, title, icon, access=None, route='', all_entries=None):
    """
    Parses a given set of entries and checks which ones the user can access.

    parameter:
    user -> kode objek dari pengguna (tipe data: auth.models.User)
    title -> judul dari link awal (index) (tipe data: str)
    icon -> icon dari link awal (index) (tipe data: str)
    access -> Memberikan akses kepada role tertentu. Kosong berarti terbuka untuk umum
    all_entries -> sub_entries dari link awal (index) (tipe data: str)

    return -> dictionary hasil dari menu entries (tipe data: dict)
    """
    if all_entries is None:
        all_entries = []
    if access is None:
        access = []
    result = {
        'title': title,
        'icon': icon
    }
    allowed_entries = []
    passed = False
    if user is not None:
        if len(route) > 0:
            result['route'] = route
            passed = len(access) == 0 or user.role in access
        else:
            for entry in all_entries:
                # make this recursive
                if len(entry['access']) == 0 or user.role in entry['access']:
                    allowed_entries.append(entry)
            if len(allowed_entries) > 0:
                result['entries'] = allowed_entries
                passed = True
    elif len(access) == 0:
        if len(route) > 0:
            result['route'] = route
            passed = True
        else:
            for entry in all_entries:
                if len(entry['access']) == 0:
                    allowed_entries.append(entry)
            if len(allowed_entries) > 0:
                result['entries'] = allowed_entries
                passed = True
    return result if passed else {}
