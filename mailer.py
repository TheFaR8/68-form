import requests


class Mailer:
    """
    Pakai API mailgun (karena mailjet terlalu lama konfirmasi :v)
    """

    sender = None
    auth = None
    api_url = None

    def __init__(self, api_key, domain, sender_name):
        """
        Constructor of the class

        parameter:
        domain -> nama domain dari sender (str)
        api_key -> api dari mailgun (str)
        sender_name -> nama dari orang pengirim (str)
        """
        self.auth = ("api", api_key)
        self.api_url = "https://api.mailgun.com/v3/{domain}".format(domain=domain)
        self.sender = "{sender} <noreply@{domain}". format(sender=sender_name, domain=domain)

    def send_simple_message(self, data):
        """
        Send a simple message

        parameter:
        data: dict mengenai data dalam email (dict)
        return: objek respon (request.Response)
        """
        data['from'] = self.sender
        return requests.post("{url}/messages".format(url=self.api_url), auth=self.auth, data=data)
