from wtforms import Form, StringField, SelectField


class SearchForm(Form):
    choices = ['NIS', 'email',
               'Nama', 'kelas',
               'jurusan1', 'jurusan2',
               'jurusan3']
    select = SelectField('Cari:', choices=choices)
    search = StringField('')
