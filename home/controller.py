"""
home Controller
===============
Di module ini, kita menjaga fungsi di homepage
"""

from home.forms import SearchForm
from flask import Blueprint, flash, request, redirect, g

from decorators import template_renderer
from auth.models import User, Role
from auth.controller import login_required, check_access_right

home = Blueprint('home', __name__)


@home.before_app_request
@template_renderer
def before_app_request():
    g.menu_entries = {
        'title': 'Home',
        'icon': 'home',
        'route': 'home.index'
    }


@home.route('/', methods=['GET', 'POST'])
@template_renderer
@login_required
@check_access_right([Role.admin])
def index():
    search = SearchForm(request.form)
    if request.method == 'POST':
        return search_results(search)

    return {
        'form': search
        }


@home.route('/results')
@template_renderer
@login_required
@check_access_right([Role.admin])
def search_results(search):
    results = []
    search_string = search.data['search']

    if search.data['search'] == '':
        qry = User.query.filter_by("{search}".format(search=search_string)).first()
        results = qry.all()

    if not results:
        flash('Tidak ditemukan', 'error_message')
        return redirect("/")
    else:
        # Menampilkan results
        return {
            results: results
        }


@home.route('/about')
@template_renderer
def about():
    return []
