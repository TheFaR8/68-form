import logging
import logging.handlers
import os


class LogConfiguration:
    """
    Log untuk semua konfigurasi dalam program
    """
    def __init__(self, folder, filename, debug=False):
        # Console handler
        self._consolelogger = logging.StreamHandler()
        self._consolelogger.setFormatter(logging.Formatter('[%(levelname)s] %(message)s '))
        if debug:
            self._consolelogger.setLevel(logging.DEBUG)
        else:
            self._consolelogger.setLevel(logging.INFO)
        # file handler
        path = os.path.join(folder, 'logs', '{name}.log'.format(name=filename))
        self._fileLogger = logging.handlers.RotatingFileHandler(path, maxBytes=1024 * 1024, backupCount=20)
        self._fileLogger.setLevel(logging.DEBUG)
        # format dari log
        formatter = logging.Formatter('[%(name)s][%(levelname)s][%(asctime)s] %(message)s')
        self._fileLogger.setFormatter(formatter)

    @property
    def file_logger(self):
        return self._fileLogger

    @property
    def console_logger(self):
        return self._consolelogger

    def create_logger(self, name):
        logger = logging.getLogger(name)
        logger.setLevel(logging.DEBUG)
        # handler di logger
        logger.addHandler(self.file_logger)
        logger.addHandler(self.console_logger)

        return logger
